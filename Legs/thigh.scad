include <../quality.scad>
use <../mx-common.scad>
use <../mx28.scad>
use <../mx64.scad>

/**
    m64 = 41w x 40.2l x 61.1h
    m28 = 35.6w x 35.5l x 50.6h
*/

/* Define constants */
HORN_N_HOLES = 8;
HORN_RING_RADIUS = 8;
HORN_HOLE_RADIUS = 1.1;

toppe_holes_dist = 22;

joint_dist=143;
thickness=6;
inner_width=41.5;

height = 80;

array28 = [[-125,-8],[-119.5,-5.7],[-117,0],[-125,8],[-119.5,5.7]];

array64 = [[-29.25, -2.7],[-29.24, -37.3],[-34.55, -9],[-34.55, -31]];

screw_hole_Iradius=1.35;
screw_hole_Eradius=2.5;

$fa = 8;
$fs = 1;

//translate([0,-141.5, 16]) import("pr13_b01_brkt_cam.stl");

module middleSupport(){
   
        translate([-22, -10.75, inner_width*1.5])  cube([inner_width+3, inner_width/2, 5]);
    
        translate([-22, -10.75, inner_width*2.2])  cube([inner_width+3, inner_width/2, 5]);
}



module sideM28() {
    difference(){  
        side();
        m28();
    }
}

union() {
    sideM28();
    
    mirror([1, 0 , 0]) sideM28();
    middleSupport();  
}

//rotate([0, -90, 0]) mx64();

module screw_hole(radius, height) {
  translate([0, 0, -1]) cylinder(r=radius, h=height + 2);
 }

module makeHoles(radius,height,p,y){
        for (i = p){
            translate(concat(i,y))screw_hole(radius,height);
        }
}

module m28(){
    // motor
          translate([0,0,height+50-5])rotate([-180, -90, 0]) mx28(with_horn=true);
    // remove the center to make space for the horn
          translate([inner_width/2, 0, height+50-5]) rotate([0,90,0]) cylinder(r=5, h=10, $fn=100);
      // make the holes   
           rotate([180,90,0])makeHoles(screw_hole_Iradius,6,array28,-26); 
           rotate([180,90,0])makeHoles(screw_hole_Eradius,3,array28,-27);      
    
}
//translate([0,0,height+50-5])rotate([-180, -90, 0]) mx28(with_horn=true);

module side() {
  union(){  
      difference(){
     translate([17.5, 0, 0]) {  
        // mx64 
        difference() {
          translate([0, -20, 9]) cube([8.5, 45, 39]);
          // shape as mx64
          rotate([0, -90, 0]) mx64();
          // remove front side
          translate([-1, -40, -8]) cube([10, 30, 30]);
            
        }
        
      }
      // make the holes   
           translate([0, -20, 9])rotate([180,90,0])makeHoles(screw_hole_Iradius,6,array64,-24.4); 
          translate([0, -20, 9])rotate([180,90,0])makeHoles(screw_hole_Eradius,3,array64,-25.4);
  }
      
      
      //add extra support
      translate([17.5, -24, 22]) cube([8.5, 4, 26]);
      //add lenght
      translate([21, -inner_width/2, 47]) cube([5, inner_width, height]);
  }
}