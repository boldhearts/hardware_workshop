include <../quality.scad>
use <../mx-common.scad>
use <../mx28.scad>
use <../mx64.scad>

/* Define constants */
HORN_N_HOLES = 8;
HORN_RING_RADIUS = 8;
HORN_HOLE_RADIUS = 1.1;

toppe_holes_dist = 22;

joint_dist=143;
inner_width=41.5;

height = 80;

array28 = [[140,-8],[134.5,-5.7],[132,0],[140,8],[134.5,5.7]];

//array28 = [[-125,-8],[-119.5,-5.7],[-117,0],[-125,8],[-119.5,5.7]];

array64 = [[-69.04,0],[-80,-11],[-87.77,7.8],[-90,0],[-72.25,-7.8],[-80,11],[-72.25,7.8]];

screw_hole_Iradius=1.35;
screw_hole_Eradius=2.5;

cable_hole=6;

thickness=5;

$fa = 8;
$fs = 1;

//motors
//translate([-0.1, 0, height])rotate([0, -90, 0]) mx64(with_horn=true);
//translate([0, 0, -57.5])rotate([0, 90, 0]) mx28(with_horn=true);

module screw_hole(radius, height) {
  translate([0, 0, -1]) cylinder(r=radius, h=height + 2);
 }

module makeHoles(radius,height,p,y){
        for (i = p){
            translate(concat(i,y))screw_hole(radius,height);
        }
}

module mx64_b(){
    difference(){
        translate([inner_width/2+3.6,-12.5,height/2])cube([5, 35, 55]);
        // remove the side part
        translate([inner_width/2+3.6,-12.5,height/2+(55/2+10)])cube([5, 15.5, 25]);
       // remove the center to make space for the horn
        translate([inner_width/2+2, 0, height]) rotate([0,90,0]) cylinder(r=5.2, h=8, $fn=100);
       // make holes 
       rotate([0,90,0])makeHoles(screw_hole_Iradius-0.05,6,array64,25);
       rotate([0,90,0])makeHoles(screw_hole_Eradius,4,array64,26.5);
    } 
}

module mx64_bracket(){
    union(){
        translate([-0.15,0,0])mx64_b();
        // front part
        difference(){
            translate([29.2,22,height/2])rotate([0,0,90])cube([5, 58.12, 55]);
            translate([22.2,22,height/2+33])rotate([0,0,90])cube([5, 45.12, 13]);
            translate([22.2,22,height/2+8])rotate([0,0,90])cube([5, 45.12, 13]);
        }
        translate([0.425,0,0])mirror([1,0,0])mx64_b();
        // base
        translate([-25.75,-12.5,height/2])cube([5, 35, 5]);    
        translate([20.75,-12.5,height/2])cube([5, 35, 5]);    
    }
}    

module sideShape() {
    linear_extrude(thickness) {
        polygon([[39.5, 0], [39.5, 39.5],[140,25],[140,0],[50,7]]);
    }
}  

module side(){
    difference(){
        // side
        translate([20.753,-12.49,79.55])rotate([0,90,0])sideShape();
        // holes for cables
        translate([23,9,25])cube([8,12,8],true); 
        translate([23,5,-15])cube([8,12,8],true);  
             
    } 
    // front panel
    translate([12,22.747,28])rotate([-8.2,0,0])cube([25,5,25],true); 
}

module mx28_b(){
    difference(){
        side();  
        translate([-1,0,82.55])rotate([180,90,0])makeHoles(screw_hole_Iradius-0.3,6,array28,-26); 
        translate([-1,0,82.55])rotate([180,90,0])makeHoles(screw_hole_Eradius,3,array28,-27);
        // remove the center to make space for the horn
        translate([20,0,-57]) rotate([0,90,0]) cylinder(r=4.5, h=10, $fn=100); 
    } 
}

module mx28_bracket(){
    union(){
       mx28_b();
       mirror([1,0,0])mx28_b();
       // add front side
        
    }
}    

union(){
    mx64_bracket();
    mx28_bracket();
}