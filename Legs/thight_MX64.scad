include <../quality.scad>
use <../mx-common.scad>
use <../mx28.scad>
use <../mx64.scad>

/* Define constants */
HORN_N_HOLES = 8;
HORN_RING_RADIUS = 8;
HORN_HOLE_RADIUS = 1.1;

toppe_holes_dist = 22;

joint_dist=143;
thickness=6;
inner_width=41.5;

height = 80;

array28 = [[-125,-8],[-119.5,-5.7],[-117,0],[-125,8],[-119.5,5.7]];

array64 = [[-29.25, -2.7],[-29.24, -37.3],[-34.55, -9],[-34.55, -31]];

screw_hole_Iradius=1.35;
screw_hole_Eradius=2.5;

cable_hole=6;

$fa = 8;
$fs = 1;

//motors
//translate([0, 0, 0])rotate([0, -90, 0]) mx64(with_horn=true);
//translate([0, 0, height+45])rotate([0, -90, 0]) mx28(with_horn=true);

module screw_hole(radius, height) {
  translate([0, 0, -1]) cylinder(r=radius, h=height + 2);
 }

module makeHoles(radius,height,p,y){
        for (i = p){
            translate(concat(i,y))screw_hole(radius,height);
        }
}

//brackets for mx64
module mx64_b(){
    difference(){
        translate([17.1, 0, 0]) {
            difference() {
                  translate([0, -20, 35]) cube([8.65, 41, 12]);
                  // shape as mx64
                  rotate([0, -90, 0]) mx64();
             }
        }
         // make the holes   
           translate([0, -20, 9])rotate([180,90,0])makeHoles(screw_hole_Iradius,6,array64,-23.4); 
          translate([0, -20, 9])rotate([180,90,0])makeHoles(screw_hole_Eradius,3,array64,-26.4);       
     }    
    //add extra support
      translate([17.1, -21, 35]) cube([8.65, 1, 12]); 
}

module bracket_mx64(){
    union(){
        mx64_b();
        mirror([1, 0 , 0])mx64_b();
        //up part
        translate([-25.75, -17.5, 52])rotate([0,90,0])cube([5, 35, 51.5]);
    }
}

//brackets for mx28
module mx28_b(){
    difference(){
        translate([inner_width/2,-17.5,height+30])cube([5, 35, 18]);
    // remove the center to make space for the horn
          translate([inner_width/2, 0, height+50-5]) rotate([0,90,0]) cylinder(r=4.4, h=10, $fn=100);
      // make the holes   
           rotate([180,90,0])makeHoles(screw_hole_Iradius,6,array28,-26); 
           rotate([180,90,0])makeHoles(screw_hole_Eradius,3,array28,-27);    
    }  
    
}    

module bracket_mx28(){
    union(){
        mx28_b();
        mirror([1, 0 , 0])mx28_b();
        
        difference(){
            // lower part
            translate([-25.75,-17.5,height])rotate([0,90,0])cube([5, 35, 51.5]);
            // make hole for cables
            translate([0, 0, 73.5]) cylinder(r=cable_hole, h=8);
    
        }
    }    
}    

module side(){
    difference(){
        translate([-25.75,-17.5,47])cube([5, 35, height-15]);
        
        // make hole for cables
        translate([-27, 0, 100]) rotate([0,90,0]) cylinder(r=cable_hole, h=8);
        translate([-27, 0, 60]) rotate([0,90,0]) cylinder(r=cable_hole, h=8);
    }
    
    // closing parts
    translate([-25.75,-12.5,52])rotate([0,0,-90])cube([5, 27, height-52-5]);
  //  translate([-25.75,17.5,52])rotate([0,0,-90])cube([5, 27, height-52-5]);
}

union(){
    side();
    mirror([1, 0 , 0])side();
    
    bracket_mx28();
    bracket_mx64();
}    