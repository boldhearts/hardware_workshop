use <mx-common.scad>
use <hn05-n102.scad>
use <hn05-i101.scad>

include <colors.scad>
include <quality.scad>

box_size = [55.5, 29, 41];

axis_offset = 13;

module mx64(with_horn) {
  color(BlackPlastic) {
    difference() {
      translate([axis_offset, 0, 0]) union() {
        difference() {
          mx_box(box_size);
          translate([0, 0, -box_size[2] / 2]) bottom_mold(box_size, height=3.5, rounding_radius=1.5);
        }
        rim(box_size, z=12.5, hole_x_offset=9, hole_distance=22);
        mirror([0, 0, 1]) rim(box_size, z=12.5, hole_x_offset=9, hole_distance=22);
      }
      translate([0, 0, -box_size[2] / 2]) cylinder(h=1, r=7.5);
    }

    translate([0, 0, -box_size[2] / 2 + 1]) {
      difference() {
        union() {
          translate([0, 0, -0.3]) cylinder(h=0.3, r=5);
          translate([0, 0, -2.5]) cylinder(h=2.5, r=4);
        }
        translate([0, 0, -2.5]) cylinder(h=4.4, r=1.6, center=true);
      }
    }
  }

  translate([0, 0, box_size[2] / 2]) {
     color(Metal) horn_connector(height=3.9, radius=4); 
  }

  if (with_horn) {
    translate([0, 0, box_size[2] / 2]) hn05_n102();
    translate([0, 0, -box_size[2] / 2]) mirror([0, 0, 1]) hn05_i101();
  }

}

mx64(with_horn=true);
