use <MCAD/boxes.scad>

/** Standard MX motor rounded box

    @param size 3D vector
 */
module mx_box(size) {
  roundedBox(size, 2, false);
}

/** Rounded bevel to fill in bottom corners

    @param box_size >= 2D vector
    @param screw_offset Scalar distance of main motor corner screws from side of motor box
    @param screw_radius Radius of head of main motor corner screw
    @param rounding_radius Radius to use for the rounded bottom edges
 */
module bottom_mold_corner_bevel(box_size, screw_offset, screw_radius, rounding_radius) {
  translate([-box_size[0] / 2 + screw_offset, -box_size[1] / 2 + screw_offset]) difference() {
    square([screw_radius + rounding_radius, screw_radius + rounding_radius]);
    circle(screw_radius + rounding_radius);
  }
}

/** Shape to cut out of bottom of motor box

    @param box_size >= 2D vector
    @param height Total height of mold (from bottom of motor to bottom of lowest rim)
    @param rounding_radius Radius to use for the rounded bottom edges
    @param screw_offset Scalar distance of main motor corner screws from side of motor box
    @param screw_radius Radius of head of main motor corner screw
 */
module bottom_mold(box_size, height, rounding_radius, screw_offset=3, screw_radius=2) {
  difference() {
    cube([box_size[0] + 5, box_size[1] + 5, height * 2], center=true);
    translate([0, 0, height]) {
      minkowski() {
        translate([0, 0, -height + rounding_radius]) {
          linear_extrude(height) union() {
            square([box_size[0] - 2 * (screw_offset + screw_radius + rounding_radius), box_size[1] - 2 * rounding_radius], center=true);
            square([box_size[0] - 2 * rounding_radius, box_size[1] - 2 * (screw_offset + screw_radius + rounding_radius)], center=true);

            mirror([0, 0, 0]) bottom_mold_corner_bevel(box_size, screw_offset, screw_radius, rounding_radius);
            mirror([1, 0, 0]) bottom_mold_corner_bevel(box_size, screw_offset, screw_radius, rounding_radius);
            mirror([0, 1, 0]) bottom_mold_corner_bevel(box_size, screw_offset, screw_radius, rounding_radius);
            mirror([1, 0, 0]) mirror([0, 1, 0]) bottom_mold_corner_bevel(box_size, screw_offset, screw_radius, rounding_radius);
          }
        }
        sphere(rounding_radius);
      }
    }
  }
}

/** A square with straight bevelled corners

    @param size 2D vector
    @param bevel_size 2D vector
 */
module beveled_square(size, bevel_size) {
  half_size = size / 2;
  polygon([
            [-half_size[0], -half_size[1] + bevel_size[1]],
            [-half_size[0] + bevel_size[0], -half_size[1]],
            [half_size[0] - bevel_size[0], -half_size[1]],
            [half_size[0], -half_size[1] + bevel_size[1]],
            [half_size[0], half_size[1] - bevel_size[1]],
            [half_size[0] - bevel_size[0], half_size[1]],
            [-half_size[0] + bevel_size[0], half_size[1]],
            [-half_size[0], half_size[1] - bevel_size[1]]
            ]);
}

/** Cutout of nut holder to hold the nut

    @param width Width along y axis (e.g. rim width)
    @param nut_width Width of nut, measured between two parallel edges
    @param nut_side Length of single edge of nut
    @param nut_height Height of nut
 */
module nut_cutout(width, nut_width, nut_side, nut_height) {
  linear_extrude(2 * nut_height, center=true)
    polygon([
              [-nut_width / 2, -width],
              [-nut_width / 2, width / 2 - cos(60) * nut_side],
              [0, width / 2],
              [nut_width / 2, width / 2 - cos(60) * nut_side],
              [nut_width / 2, -width]
              ]);
}

/** Main plane of motor attachment rim

    @param size 2D vector
    @param height Height of plane
 */
module rim_main(size, height) {
  linear_extrude(height) square(size, center=true);
}

/** Locations of hole centers in motor attachment rim

    @param rim_size 2D vector
    @param hole_x_offset Distance of first side holes from side with minimum X
    @param hole_distance Distance between two holes
    @param hole_side_offset Distance from holes to edge of rims

    @returns List of 3 lists of x-y coordinates: for holes on negative
    Y side (3x), positive Y side (3x), and goles on positive X side (2x)
 */
function rim_holes_xy(rim_size, hole_x_offset, hole_distance, hole_side_offset) =
  [
    [for (i = [0:2]) [-rim_size[0] / 2 + hole_x_offset + i * hole_distance, -rim_size[1] / 2 + hole_side_offset]],
    [for (i = [0:2]) [-rim_size[0] / 2 + hole_x_offset + i * hole_distance, rim_size[1] / 2 - hole_side_offset]],
    [[rim_size[0] / 2 - hole_side_offset, -hole_distance / 2], [rim_size[0] / 2 - hole_side_offset, hole_distance / 2]]
    ];

/** Rim used to attach parts to motor

    @param box_size 3D vector
    @param hole_x_offset Distance of first side holes from side with minimum X
    @param hole_distance Distance between two holes    
 */
module rim(box_size, z, hole_x_offset, hole_distance,
           width=5.6, height=4.5, nut_holder_height=2.5, nut_height=2, hole_radius=1.35) {
  // Determine full rim size and hole and nut holder parameters
  size = [box_size[0] + width, box_size[1] + 2 * width];
  hole_offset = width / 2;
  nut_side = width / (1 + 2 * sin(30));
  nut_width = 2 * nut_side * cos(30);
  nut_holder_width = nut_width + 2 * 1.5;

  holes_xy = rim_holes_xy([size[0], size[1]], hole_x_offset, hole_distance, hole_offset);

  // Translate so that back (negative X) is touching back of box
  translate([hole_offset, 0, z]) {
    intersection() {
      difference() {
        union() {
          // Main plane
          translate([0, 0, nut_holder_height]) rim_main(size, height- nut_holder_height);

          // Filler to prevent gaps at rounded box corners
          linear_extrude(nut_holder_height) square([box_size[0] - 2 * hole_offset, box_size[1]], center=true);

          // Little boxes to cut the nut holders out
          // 3 side holes on each side
          for (xy = concat(holes_xy[0], holes_xy[1]))
            translate(xy)
              linear_extrude(nut_holder_height) square([nut_holder_width, width], center=true);
          // 2 front holes
          for (xy = concat(holes_xy[2]))
            translate(xy)
              linear_extrude(nut_holder_height) square([width, nut_holder_width], center=true);
        }

        // Cut out nut holders
        for (xy = concat(holes_xy[0]))
          translate(xy) nut_cutout(width, nut_width, nut_side, nut_height);
               
        for (xy = concat(holes_xy[1]))
          translate(xy) rotate(180) nut_cutout(width, nut_width, nut_side, nut_height);

        for (xy = concat(holes_xy[2]))
          translate(xy) rotate(90) nut_cutout(width, nut_width, nut_side, nut_height);

        // Make screw holes
        for (xy = concat(holes_xy[0], holes_xy[1], holes_xy[2]))
          translate(concat(xy, [height / 2])) cylinder(h=1.2 * height, r=hole_radius, center=true);
      }

      // Cut off corners
      linear_extrude(height) beveled_square(size, [hole_x_offset -1, width]);
    }
  }
}

/** Ridged connector to attach horn to axel

    @param height Connector height
    @param radius Connector radius, measured at ridge tips
 */
module horn_connector(height, radius) {
  n_ridges = 30;
  ridge_depth = 0.25;
  ridge_radius = radius - ridge_depth;
  angle_step = 360 / n_ridges;
  half_angle_step = angle_step / 2;

  ridges = [for (i = [0:n_ridges])
      [[sin(i * angle_step) * radius, cos(i * angle_step) * radius],
       [sin((i + 0.5) * angle_step) * (ridge_radius), cos((i + 0.5) * angle_step) * ridge_radius]]
    ];
  linear_extrude(height) polygon([for (a = ridges) for (b = a) b]);
}

/** Flat washer
 */
module washer(outer_radius, inner_radius, height) {
  difference() {
    cylinder(h=height, r=outer_radius);
    cylinder(h=2 * height + 0.1, r=inner_radius, center=true);
  }
}

/** Ring of number of evenly spaced holes
 */
module hole_ring(n_holes, ring_radius, hole_radius, hole_depth) {
  for (i = [0:n_holes])
    translate([sin(i * 360 / n_holes) * ring_radius, cos(i * 360 / n_holes) * ring_radius]) cylinder(h=hole_depth, r=hole_radius);
}

/** Ring of number of evenly spaced holes
 */
module hole_ring_2d(n_holes, ring_radius, hole_radius) {
  for (i = [0:n_holes])
    translate([sin(i * 360 / n_holes) * ring_radius, cos(i * 360 / n_holes) * ring_radius]) circle(r=hole_radius);
}
