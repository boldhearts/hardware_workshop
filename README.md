# hardware_workshop

Requirements and guides for 3D printing of robots' hardware - RoboCup Humanoid League 

# directories

Each directory contains 3D model examples of the Bold Bot hardware, which it is based on Darwin OP. The Darwin OP's assembly manual and components are available and can be download [here](https://sourceforge.net/projects/darwinop/files/Hardware/Mechanics/Physical%20Information/).

# complaints to rules

Humanoid League's robots need to comply with the League's rules which can be found on [Humanoid League website](http://humanoid.robocup.org/wp-content/uploads/RC-HL-2022-Rules-1.pdf).
