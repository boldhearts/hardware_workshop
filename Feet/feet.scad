include <../colors.scad>
include <../quality.scad>

width=93.48;
length=140.22;
darwin_foot_width=66;
darwin_foot_length=104;
foot_x_offset=(width - darwin_foot_width) / 2 - 5;
foot_y_offset=(length - darwin_foot_length) / 2 - 5;

translate([darwin_foot_width / 2 + 10, 0]) mirror([0, 0, 1]) nubot_plate(foot_x_offset=foot_x_offset, foot_y_offset=foot_y_offset);
mirror([1, 0, 0]) translate([darwin_foot_width / 2 + 10, 0]) mirror([0, 0, 1]) nubot_plate(foot_x_offset=foot_x_offset, foot_y_offset=foot_y_offset);

module nubot_plate(width=93.48, length=140.22, height=6,
                   foot_x_offset=0, foot_y_offset=0,
                   stud_hole_offset=7.5, stud_hole_radius=2.5,
                   stud_bolt_head_width=4.1, stud_bolt_head_height=3.6,
                   screw_hole_radius=1.65, screw_head_radius=3, screw_head_height=2.2) {
  
  screw_hole_positions = [
    [2.5, -17.5],
    [18.5, 0],
    [2.5, 17.5]
    ];

  stud_hole_positions = calc_stud_hole_positions(width, length, stud_hole_offset);
        
  //color(BlackPlastic) {
    difference() {
      union() {
        foot_frame(width, length, height, foot_x_offset, foot_y_offset, stud_hole_offset, 0);
        translate([0, 0, -height]) {
          difference() {
            foot_frame(width, length, height, foot_x_offset, foot_y_offset, stud_hole_offset, 5);
            translate([0, 0, height / 2])
              cube([darwin_foot_width, darwin_foot_length, height], center=true);
            %translate([0, 0, height / 2])
              cube([darwin_foot_width, darwin_foot_length, height], center=true);
          }
        }
        linear_extrude(height) {
          hull() {
            for (p = screw_hole_positions) {
              translate(p) circle(r=screw_head_radius + 3);
              mirror([1, 0, 0]) translate(p) circle(r=screw_head_radius + 3);
            }
          }
        }
      }

 
      for (p = screw_hole_positions) {
        translate(p) screw_hole(screw_hole_radius, height, screw_head_radius, screw_head_height);
        mirror([1, 0, 0]) translate(p) screw_hole(screw_hole_radius, height, screw_head_radius, screw_head_height);
      }

      translate([foot_x_offset, foot_y_offset]) {
        for (p = stud_hole_positions) {
          translate(p) {
            translate([0, 0, -10]) {
              cylinder(r=stud_hole_radius, h=30);
              cylinder(r=stud_bolt_head_width/sin(60), h=stud_bolt_head_height + 11, $fn=6);
            }
          }
        }
      }
    }

  //}
 
}

module foot_frame(width=93.48, length=140.22, height=6,
                  foot_x_offset=0, foot_y_offset=0,
                  stud_hole_offset=7.5, bar_width_reduction=0) {
  stud_hole_positions = calc_stud_hole_positions(width, length, stud_hole_offset);

  intersection() { 
    linear_extrude(height) {
      for (p = stud_hole_positions) {
        hull() {
          circle(r=stud_hole_offset - bar_width_reduction);
          translate([foot_x_offset, foot_y_offset])
            translate(p)
            circle(r=stud_hole_offset - bar_width_reduction);
      
        }
      }

      p0 = stud_hole_positions[0];
      p1 = stud_hole_positions[1];
      p2 = stud_hole_positions[2];
      p3 = stud_hole_positions[3];
      l = p1[1] - p2[1];
      x = 1 / (16 * stud_hole_offset) * l * l - 1;
      r = x + stud_hole_offset;
          
      translate([foot_x_offset, foot_y_offset]) {
        /*
        difference() {
          translate([p1[0] - x, p2[1] + l / 2]) circle(r=r + 2 * stud_hole_offset - bar_width_reduction);
          translate([p1[0] - x, p2[1] + l / 2]) circle(r=r + bar_width_reduction);
        }
        */
        
        difference() {
          translate([p0[0] + x, p3[1] + l / 2]) circle(r=r + 2 * stud_hole_offset - bar_width_reduction);
          translate([p0[0] + x, p3[1] + l / 2]) circle(r=r + bar_width_reduction);
        }

        hull() {
          translate(p1) circle(r=stud_hole_offset - bar_width_reduction);
          translate(p2) circle(r=stud_hole_offset - bar_width_reduction);
        }

        hull() {
          translate(p0) circle(r=stud_hole_offset - bar_width_reduction);
          translate(p1) circle(r=stud_hole_offset - bar_width_reduction);
        }
        hull() {
          translate(p2) circle(r=stud_hole_offset - bar_width_reduction);
          translate(p3) circle(r=stud_hole_offset - bar_width_reduction);
        }
      }
    }
        
    translate([foot_x_offset, foot_y_offset]) {
      foot_extents(width - 2 * bar_width_reduction, length - 2 * bar_width_reduction, height,
                   stud_hole_offset - bar_width_reduction);
    }
  }
}

module foot_extents(width=93.48, length=140.22, height=6,
                   stud_hole_offset=7.5) {
  
  stud_hole_positions = calc_stud_hole_positions(width, length, stud_hole_offset);

  linear_extrude(height) {
    hull() {
      for (p = stud_hole_positions) {
        translate(p) {
          circle(r=stud_hole_offset);
        }
      }
    }
  }
}


function calc_stud_hole_positions(width, length, stud_hole_offset) =
  [
    [width / 2 - stud_hole_offset, length / 2 - stud_hole_offset],
    [-width / 2 + stud_hole_offset, length / 2 - stud_hole_offset],
    [-width / 2 + stud_hole_offset, -length / 2 + stud_hole_offset],
    [width / 2 - stud_hole_offset, -length / 2 + stud_hole_offset]
    ];

module screw_hole(radius, height, head_radius, head_height) {
  translate([0, 0, -1]) cylinder(r=radius, h=height + 2);
  translate([0, 0, height-head_height]) cylinder(r=head_radius, h=head_height + 1, $fn = 100);
 }
