include <../quality.scad>

/** Get the x/y coordinates of the corners of the cover
 *
 * @returns list of length 3, with coordinates for back, middle, and tip corners
 */
function corners_xy(thickness=3, widening=5, arm_length=130) =
  [[-30 + thickness / 2, 18 + thickness / 2],
   [30 + thickness / 2, 18 + widening + thickness / 2],
   [arm_length - 30 - thickness / 2, 12.5 + thickness / 2]];

/** Create outer plate
 */
module base_plate(thickness=3, widening=5) {
  ht = thickness / 2;
  cs = corners_xy(thickness, widening);

  // We extrude the corners, then take the hull to get the full plate
  hull() translate([0, 0, -thickness]) linear_extrude(thickness) {
    translate(cs[0]) circle(r=ht, center=true);
    translate(cs[1]) circle(r=ht, center=true);

    mirror([0, 1]) {
      translate(cs[0]) circle(r=ht, center=true);
      translate(cs[1]) circle(r=ht, center=true);
    }
  }
}

/** Create outer walls of base and extension
 */
module side_wall(thickness=3, widening=5, arm_length=130) {
  ht = thickness / 2;
  cs = corners_xy(thickness, widening, arm_length);

  // Base wall
  hull() translate([0, 0, -thickness]) linear_extrude(20 + thickness) {
    translate(cs[0]) circle(r=ht, center=true);
    translate(cs[1]) circle(r=ht, center=true);
  }
  // Extension wall
  hull() {
    translate([0, 0, -thickness]) {
      linear_extrude(20 + thickness) translate(cs[1]) circle(r=ht, center=true);
      linear_extrude(5) translate(cs[2]) circle(r=ht, center=true);
    }
  }
}

/** Create cross support bars of extension
 */
module cross_bar(thickness=3, widening=5, base_offset=9, tip_height=5, arm_length=130) {
  ht = thickness / 2;
  cs = corners_xy(thickness, widening, arm_length);

  hull() {
    translate([0, 0, -thickness]) {
      linear_extrude(base_offset + 5 + thickness) translate(cs[1]) circle(r=ht, center=true);
      linear_extrude(tip_height) mirror([0, 1]) translate(cs[2]) circle(r=ht, center=true);
    }
  }
}

/** Create tip bar
 */
module tip(thickness=3, height=5, arm_length=130, corner_radius=10) {
  ht = thickness / 2;
  cs = corners_xy(thickness, arm_length=arm_length);

  translate([cs[2][0], 0, -thickness]) {
    linear_extrude(height) difference() {
        hull() {
            translate([0, cs[2][1] + ht - corner_radius]) circle(r=corner_radius, center=true);
            mirror([0, 1]) translate([0, cs[2][1] + ht - corner_radius]) circle(r=corner_radius, center=true);
        }
        translate([-corner_radius, 0]) square([corner_radius, 3 * cs[2][1]], center=true);
    }
  } 
}

module screw_support(thickness=3, widening=5, radius=4, wall_thickness=1.5, base_offset=9) {
  // Screw supports
  for (x = [-14.5, 14.5], y = [-12.5, 12.5])
    translate([x, y])
      cylinder(h=base_offset, r=radius, center=false);
  
  // Walls
  translate([-14.5, 0, base_offset/2]) cube([wall_thickness, 36 + 2 * widening / 4, base_offset], center=true);
  translate([14.5, 0, base_offset/2]) cube([wall_thickness, 36 + 2 * widening * 3 / 4, base_offset], center=true);
  translate([0, -12.5, base_offset/2]) cube([60, wall_thickness, base_offset], center=true);
  translate([0, 12.5, base_offset/2]) cube([60, wall_thickness, base_offset], center=true);
}

module screw_holes(large_radius=1.8, small_radius=1.2, base_offset=9) {
  for (x = [-14.5, 14.5], y = [-12.5, 12.5]) translate([x, y]) {
      // Hole for screw thread
      translate([0, 0, 4.5]) cylinder(h=20, r=small_radius, center=true);
      // Hole for screw head
      cylinder(h=(base_offset - 1.5) * 2, r=large_radius, center=true);
    }
}

module armcover(thickness=3, widening=5, arm_length=130, base_offset=9, tip_height=5, tip_corner_radius=10,
                screw_holes_large_radius=1.8, screw_holes_small_radius=1.2) {
  difference() {
    union() {
      base_plate(thickness, widening);
      
      side_wall(thickness, widening, arm_length);
      mirror([0, 1]) side_wall(thickness, widening, arm_length);
      
      cross_bar(thickness, widening, base_offset, tip_height, arm_length);
      mirror([0, 1]) cross_bar(thickness, widening, base_offset, tip_height, arm_length);
      
      tip(thickness, tip_height, arm_length, tip_corner_radius);
      
      screw_support(thickness, widening, base_offset=base_offset);
    }
    
    screw_holes(screw_holes_large_radius, screw_holes_small_radius, base_offset);
  }
}

thickness = 3;
base_offset = 4;

//translate([45.75 - 30, 0, base_offset - thickness / 2 + 15]) rotate([0, 90, 90]) import("../../lower_arm_bracket.stl");
armcover(thickness=thickness, widening=3, arm_length=140, base_offset=base_offset, tip_height=5, tip_corner_radius=10,
         screw_holes_large_radius=1.8, screw_holes_small_radius=1.2);
